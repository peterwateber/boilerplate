import express from 'express'
var app = express()

app.set('view engine', 'jade');
app.use(express.static('public'));

app.get('/', function (req, res) {
  res.render('index')
});

//DEVELOPMENT
app.locals.pretty = true;


var server = app.listen(3000, function () {
  var port = server.address().port
  console.log(port)
});
