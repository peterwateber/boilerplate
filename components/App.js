import React from 'react';

class App extends React.Component {
    onClick() {
        alert(1);
    }
    render() {
        return <div onClick={this.onClick}>Hello World!</div>;
    }
}

export default App;
