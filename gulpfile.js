var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    webpack = require('webpack-stream');


gulp.task('react-webpack', function() {
    return gulp.src('components/**.js')
        .pipe(webpack({
            watch: true,
            entry: {
                app: './components/render.js'
            },
            output: {
                filename: 'build.js'
            },
            module: {
                loaders: [{
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                }]
            }
        }))
        .pipe(gulp.dest('public/js/'));
});

gulp.task('nodemon', function() {
    nodemon({
        script: './server.js',
        exec: 'babel-node',
        ext: 'js',
        env: {
            'NODE_ENV': 'development'
        },
        ignore: [
            'node_modules/'
        ],
    }).on('restart', function() {
        console.log('Restarted!');
    });
});

gulp.task('default', ['react-webpack', 'nodemon']);
